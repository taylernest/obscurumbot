# ObscurumBot
### Information
Advanced IRC bot ObscurumBot<br>
Written on PircBot, this bot is very usable and well documented<br>
You can make your own suggestions if you are not programmer. Or you can fork from main development.<br>
Bot can be connected to any IRC server, including Twitch (use OAuth pass).<br>
Also, it has simple and flexible GUI!<br>
## <a href=https://goo.gl/dS7e5N/>Download</a>
### Recommendations
We recommend to name command-multicontainer classes with AC(Command Name) and simple command classes with C(Command Name). Also finalizing command is highly recommended: do not extend command classes.<br>
Each new command class should be in org.obscurasoft.obscurumbot.command package, extend AbstractCommand.java class and
be added to class array in BotHelper.java<br>
If you expecting problems, we understand your problem. 
Visit <a href=https://github.com/refactoral/obscurumbot/issues/45>this issue</a>
### Prerequisites
Requires Java 8<br>
PircBot and some other libs included in sources (libs dir)<br>
### Contact with us
https://github.com/refactoral - Github group<br>
http://refactoral.tk/ - Our site. RUSSIAN.<br>
https://vk.com/refactoral - Our group VKontakte. RUSSIAN.<br>
### Licensed under Apache License 2.0
