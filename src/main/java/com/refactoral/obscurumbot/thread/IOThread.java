package com.refactoral.obscurumbot.thread;

import java.io.File;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.ObscurumBot;

/**
 * Input/Output thread class.
 * @author GodusX
 */
public class IOThread extends Thread {

	public boolean hasWork;
	public boolean save;

	public IOThread() {
		super("[IOThread] Command save/load");
		this.setDaemon(true);
		this.setPriority(NORM_PRIORITY);
	}

	public void run() {
		while (true) {
			if (!hasWork) {
				try {
					sleep(2000);
				} catch (Exception e) {
					continue;
				}
			} else {
				synchronized (Main.obot) {
					if (save) {
						File f = new File(
								ObscurumBot.class.getProtectionDomain().getCodeSource().getLocation().getPath())
										.getParentFile();

						for (AbstractCommand c : ObscurumBot.command)
							c.onDataSave(f);
					} else {
						File f = new File(
								ObscurumBot.class.getProtectionDomain().getCodeSource().getLocation().getPath())
										.getParentFile();

						for (AbstractCommand c : ObscurumBot.command)
							c.onDataLoad(f);
					}

					save = false;
					hasWork = false;
					continue;
				}
			}
		}
	}
}
