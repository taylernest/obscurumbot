package com.refactoral.obscurumbot.thread;

import com.refactoral.obscurumbot.Main;

/**
 * Tick thread class.
 * 
 * @author GodusX
 */
public class BotTick extends Thread {
	public static boolean cont = false;
	public static boolean loop = false;

	public BotTick() {
		super("BotTick");
		cont = true;
		this.setDaemon(true);
	}

	public void run() {
		super.run();
		while (cont) {
			loop = false;
			try {
				synchronized (Main.obot) {
					Main.obot.mainLoop();
				}
				loop = true;
				sleep(991L);
				while(!SecondTick.loop) {
					
				}
			} catch (Exception e) {
				cont = false;
			}
		}
	}
}
