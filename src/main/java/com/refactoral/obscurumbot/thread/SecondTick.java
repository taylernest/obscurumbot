package com.refactoral.obscurumbot.thread;

import com.refactoral.obscurumbot.Main;

public class SecondTick extends Thread {
	public static boolean cont = false;
	public static boolean loop = false;

	public SecondTick() {
		super("SecondTick");
		cont = true;
		this.setDaemon(true);
	}

	public void run() {
		super.run();
		while (cont) {
			loop = false;
			
			try {
				synchronized (Main.obot) {
					Main.obot.secondLoop();
				}
				loop = true;
				sleep(999L);
				while(!BotTick.loop) {
				}
			} catch (Exception e) {
				cont = false;
			}
		}
	}
}
