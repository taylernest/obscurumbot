package com.refactoral.obscurumbot.core.utils.crypto;

import javax.crypto.SecretKey;

public class DESSecretKey implements SecretKey {

	private final byte[] key;

	/**
	 * ключ должен иметь длину 8 байт
	 */
	public DESSecretKey(byte[] key) {
		this.key = key;
	}

	@Override
	public String getAlgorithm() {
		return "DES";
	}

	@Override
	public String getFormat() {
		return "RAW";
	}

	@Override
	public byte[] getEncoded() {
		return key;
	}
}
