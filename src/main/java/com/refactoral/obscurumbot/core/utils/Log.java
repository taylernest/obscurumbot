package com.refactoral.obscurumbot.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.refactoral.obscurumbot.Main;

public class Log {
	private static final Logger log = LogManager.getLogger("Bot");

	/**
	 * Log some info
	 * 
	 * @param msg
	 *            - message to log
	 */
	public static void info(Object msg) {
		log.info(msg);
		if (Main.obot != null) {
			String message = "[INFO] " + msg;
			Main.obot.onLog(message);
		}
	}

	/**
	 * Log warning
	 * 
	 * @param msg
	 *            - message to log
	 */
	public static void warn(Object msg) {
		log.warn(msg);
		if (Main.obot != null) {
			String message = "[WARN] " + msg;
			Main.obot.onLog(message);
		}
	}

	/**
	 * Log error info
	 * 
	 * @param msg
	 *            - message to log
	 */
	public static void error(Object msg) {
		log.error(msg);
		if (Main.obot != null) {
			String message = "[ERROR] " + msg;
			Main.obot.onLog(message);
		}
	}

	/**
	 * Log info and exit
	 * 
	 * @param msg
	 *            - message to log
	 */
	public static void fatal(Object msg) {
		log.fatal(msg);
	}
}
