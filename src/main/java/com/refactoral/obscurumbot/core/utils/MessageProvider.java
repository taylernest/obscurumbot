package com.refactoral.obscurumbot.core.utils;

import com.refactoral.obscurumbot.Main;

/**
 * Utilities for message handling between classes.
 * @author GodusX
 *
 */
public class MessageProvider {
	static int messages = 0;
	private static String message = null;
	private static String sender = null;
	
	/**
	 * Checks message for text
	 * @param text
	 * @return true if message contains @param text
	 */
	public boolean contains(String text) {
		return message.toLowerCase().contains(text.toLowerCase());
	}
	
	/**
	 * Overloaded contains() method
	 * Checks for sender and text in message
	 * @param text
	 * @param sender
	 * @return true if message contains @param text and sender is @param sender 
	 */
	public boolean contains(String text, String sender) {
		return this.contains(text) && MessageProvider.sender.equalsIgnoreCase(sender);
	}
	
	/**
	 * @return message field
	 */
	public String getMessage() {
		return MessageProvider.message;
	}
	
	/**
	 * @return sender field
	 */
	public String getSender() {
		return MessageProvider.sender;
	}

	/**
	 * Set message and sender to @param message and @param sender
	 */
	public static void setItem(String message, String sender) {
		MessageProvider.message = message;
		messages++;
		MessageProvider.sender = sender;
	}
	
	/**
	 * @return message count
	 */
	public static int getMessages() {
		return MessageProvider.messages;
	}

	/**
	 * Send message to chat
	 */
	public void Send(String message) {
		BotHelper.messageBot(message);
		Main.obot.delay = BotHelper.delay;
	}
	
}
