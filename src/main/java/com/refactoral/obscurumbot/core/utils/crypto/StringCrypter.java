package com.refactoral.obscurumbot.core.utils.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

import com.refactoral.obscurumbot.core.struct.Key;
import com.refactoral.obscurumbot.core.utils.Log;

/**
 * Encrypting and decrypting strings http://commons.apache.org/codec/
 * 
 * @author GodusX
 */
public class StringCrypter {

	public StringCrypter() {
		File f = new File("data/key.dat");
		if (f.exists() && !f.isDirectory()) {
			Log.info("Key found");
			try {
				FileInputStream fis = new FileInputStream(f);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Key k = (Key) ois.readObject();
				ois.close();
				fis.close();
				ois = null;
				fis = null;
				updateSecretKey(new DESSecretKey(k.key));
				k.key = null;
				k = null;
			} catch (Exception e1) {
				e1.printStackTrace();
				Log.fatal("Cryptography error");
				System.exit(1);
			}
		} else {
			try {
				if (f.isDirectory()) {
					f.delete();
				}
				f.createNewFile();
				Log.info("Creating new key");
				FileOutputStream fos = new FileOutputStream(f);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				try {
					Key k = new Key();
					k.key = new byte[8];
					try {
						k.key = new QuantumRandom(8).getBytes();
					} catch (Exception e) {
						Log.warn("Can't get random quantum bytes! Using standart method");
						SecureRandom srandom = new SecureRandom();
						srandom.setSeed(System.nanoTime());
						srandom.nextBytes(k.key);
						srandom = null;
					}
					
					oos.writeObject(k);
					oos.close();
					fos.close();
					oos = null;
					fos = null;
					updateSecretKey(new DESSecretKey(k.key));
					k.key = null;
					k = null;
				} catch (Exception e) {
					e.printStackTrace();
					Log.fatal("Cryptography error");
					System.exit(1);
				}
			} catch (IOException e) {
				Log.fatal(e.getMessage());
				System.exit(1);
			}
			f = null;
		}
	}

	private void updateSecretKey(SecretKey key)
			throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
		ecipher = Cipher.getInstance(key.getAlgorithm());
		dcipher = Cipher.getInstance(key.getAlgorithm());
		ecipher.init(Cipher.ENCRYPT_MODE, key);
		dcipher.init(Cipher.DECRYPT_MODE, key);
	}

	private Cipher ecipher;
	private Cipher dcipher;

	/**
	 * Encrypting method
	 *
	 * @param str
	 *            string to encrypt
	 * @return encrypted string
	 */
	public String encrypt(String str) {
		try {
			byte[] utf8 = str.getBytes("UTF8");
			byte[] enc = ecipher.doFinal(utf8);
			return Base64.encodeBase64String(enc);
		} catch (IllegalBlockSizeException | BadPaddingException | UnsupportedEncodingException ex) {
			Logger.getLogger(StringCrypter.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	/**
	 * Decrypting method
	 *
	 * @param str
	 *            encrypted string
	 * @return decrypted string
	 */
	public String decrypt(String str) {
		try {
			byte[] dec = Base64.decodeBase64(str);
			byte[] utf8 = dcipher.doFinal(dec);
			return new String(utf8, "UTF8");
		} catch (IllegalBlockSizeException | BadPaddingException | IOException ex) {
			Logger.getLogger(StringCrypter.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}
}