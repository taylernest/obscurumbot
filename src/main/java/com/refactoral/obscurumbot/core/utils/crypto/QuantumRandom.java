package com.refactoral.obscurumbot.core.utils.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class QuantumRandom {
	private DataInputStream in;
	private String page;
	private String bytes;
	private int numberOfBytes;

	public QuantumRandom() {
		this.in = null;
		this.page = "";
		this.bytes = "";
		this.numberOfBytes = 0;
	}

	public QuantumRandom(int numberOfBytes) {
		this.numberOfBytes = numberOfBytes;
		this.in = null;
		this.page = "";
		this.bytes = "";
	}

	/**
	 * Set the number of bytes.
	 * 
	 * @param numberOfBytes
	 *            The new number of bytes
	 */
	public void setNumberOfBytes(int numberOfBytes) {
		this.numberOfBytes = numberOfBytes;
	}

	/**
	 * Returns the number of bytes.
	 * 
	 * @return The number of bytes
	 */
	public int getNumberOfBytes() {
		return this.numberOfBytes;
	}

	/**
	 * Runs to methods to download and parse the webpage. Returns the bytes from
	 * the webpage.
	 * 
	 * @return The downloaded bytes
	 * @throws IOException
	 * @throws MalformedURLException
	 */
	public byte[] getBytes() throws MalformedURLException, IOException {
		byte[] temp = new byte[this.numberOfBytes];
		String store = "";

		while (store.length() < this.numberOfBytes) {
			this.getPage();
			this.parsePage();
			store += this.bytes;
		}
		byte[] storeTemp = store.getBytes();
		for (int i = 0; i < this.numberOfBytes; i++) {
			temp[i] = storeTemp[i];
		}

		return temp;
	}

	/**
	 * Downloads the webpage and stores it in memory.
	 * 
	 * @throws MalformedURLException
	 * @throws IOException
	 */
	public void getPage() throws MalformedURLException, IOException {
		try {
			URL u = new URL("http://150.203.48.55/RawChar.php");
			this.in = new DataInputStream(new BufferedInputStream(u.openStream()));
			String temp = "";
			BufferedReader d = new BufferedReader(new InputStreamReader(in));
			while ((temp = d.readLine()) != null) {
				this.page += temp;
			}
		} finally {
			try {
				if (this.in != null) {
					this.in.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Parses the random bytes from the webpage.
	 */
	public void parsePage() {

		int start;
		int end;

		start = this.page.indexOf("<table class=\"rng\" cellpadding=\"10\"> <tr><td>");
		end = this.page.indexOf("</td></tr></table><br />", start);
		start += 45;

		this.bytes = this.page.substring(start, end);

	}
}
