package com.refactoral.obscurumbot.core.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.command.ACGreetings;
import com.refactoral.obscurumbot.command.ACResponse;
import com.refactoral.obscurumbot.command.CAkella;
import com.refactoral.obscurumbot.command.CCat;
import com.refactoral.obscurumbot.command.CDate;
import com.refactoral.obscurumbot.command.CDelay;
import com.refactoral.obscurumbot.command.CFacepalm;
import com.refactoral.obscurumbot.command.CGoto;
import com.refactoral.obscurumbot.command.CIgnore;
import com.refactoral.obscurumbot.command.CLoad;
import com.refactoral.obscurumbot.command.CMarina;
import com.refactoral.obscurumbot.command.CMistik;
import com.refactoral.obscurumbot.command.CPoll;
import com.refactoral.obscurumbot.command.CReload;
import com.refactoral.obscurumbot.command.CSave;
import com.refactoral.obscurumbot.command.CTop;
import com.refactoral.obscurumbot.command.CWallet;
import com.refactoral.obscurumbot.command.CWalletInfo;
import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.utils.json.Json;
import com.refactoral.obscurumbot.core.utils.json.JsonObject;

/**
 * Some utilities for bot
 * 
 * @author GodusX
 *
 */
public class BotHelper {
	public static final String owner = "godusx";
	public static final String[] mods = new String[] { owner, "corporation_myth" };
	public static List<String> ignore = new ArrayList<String>();
	public static int delay = 5;
	public static String poll;

	public static String getBotChannel() {
		return Main.obot.lastChannel;
	}

	public static String getLastSender() {
		return Main.obot.lastSender;
	}

	public static void messageBot(Object message) {
		ObscurumBot.messagesToDeliver.add(message.toString());
	}

	public static Class<?>[] getClasses() {
		Class<?>[] arr = { ACGreetings.class, ACResponse.class, CCat.class, CDate.class, CDelay.class, CFacepalm.class,
				CGoto.class, CIgnore.class, CLoad.class, CPoll.class, CReload.class, CSave.class, CWallet.class,
				CWalletInfo.class, CMarina.class, CMistik.class, CAkella.class, CTop.class };
		return arr;
	}

	public static void delete(File dir) {
		if (!dir.isDirectory()) {
			dir.delete();
			return;
		}
		String[] entries = dir.list();
		for (String s : entries) {
			File currentFile = new File(dir.getPath(), s);
			if (currentFile.isDirectory()) {
				delete(currentFile);
			}
			currentFile.delete();
		}
		dir.delete();
	}

	private static String TWITCH_STREAM = "https://api.twitch.tv/kraken/streams/$c$";

	private static String insertChannel(String url, String channel) {
		return url.replace("$c$", channel);
	}

	public static boolean isStreamLive() {
		try {
			URL url = new URL(insertChannel(TWITCH_STREAM, Main.obot.lastChannel));
			URLConnection conn = url.openConnection();
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine = br.readLine();
			br.close();
			JsonObject jsonObj = Json.parse(inputLine).asObject();
			return (jsonObj.get("stream").isNull()) ? false : true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
