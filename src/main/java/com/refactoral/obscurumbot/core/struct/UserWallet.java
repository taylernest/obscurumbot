package com.refactoral.obscurumbot.core.struct;

import java.io.Serializable;

public class UserWallet implements Serializable {
	public long wallet = 0;
	public long delay = 900;
	public long smalldelay = 300;
	public long counter = 0;
	public boolean cont = true;
	public long smallcounter = 0;
	public String nick;
	private int bytes;
	public UserWallet(String nick) {
		this.nick = nick;
	}
	
	private void writeObject(java.io.ObjectOutputStream stream) throws java.io.IOException {
		bytes = (int) counter;
		wallet = wallet >> bytes;
		stream.defaultWriteObject();
	}

	private void readObject(java.io.ObjectInputStream stream) throws java.io.IOException, ClassNotFoundException {
		stream.defaultReadObject();
		wallet = wallet << bytes;
	}
}
