package com.refactoral.obscurumbot.core.struct;

import java.io.Serializable;

public class Key implements Serializable {
	public byte[] key = new byte[8];
}
