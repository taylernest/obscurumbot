package com.refactoral.obscurumbot.core.struct;

import java.io.Serializable;

public class Locale implements Serializable {
	public String EnterLoginData, Server, Port, Login, Password, Channel, SaveToFile, LoadFromFile, BLogin, ToTray,
			Exit, Enable, Disable, Reload, Ignore, Unignore, OpenLog, LoadData, SaveData, Autosave, Interval,
			intervalsuffix, intervalsuffix2, Send, IKnow, DangerZone, DeleteSaved, DeleteSerialized, DeleteAll,
			GenerateNew, BotLabel, poll, locale, ChangeServer, ChangePassword, ChangeLogin, ChangeChannel, Cfg, Before,
			LogFrame, CloseLog;
	public String[] statuses;

	public Locale(String locale) {
		if (locale.equalsIgnoreCase("RU")) {
			EnterLoginData = "Введите данные входа";
			Server = "Сервер:";
			Port = "Порт:";
			Login = "Логин:";
			Password = "Пароль:";
			Channel = "Канал:";
			SaveToFile = "Сохранить в файл";
			LoadFromFile = "Загрузить";
			BLogin = "Войти";
			ToTray = "В трей";
			Exit = "Выход";
			BotLabel = "Статус:";
			GenerateNew = "Новый квантовый ключ";
			DeleteAll = "Удалить ВСЕ данные бота";
			DeleteSerialized = "Удалить сериализированные данные";
			DeleteSaved = "Удалить данные авторизации";
			DangerZone = "Опасная зона!";
			IKnow = "Я знаю что делаю";
			Send = "Отправить";
			intervalsuffix2 = "мин.";
			intervalsuffix = "сек.";
			Interval = "Интервал:";
			Autosave = "Автосохранение";
			OpenLog = "Открыть лог";
			CloseLog = "Закрыть лог";
			Unignore = "Убрать игнор";
			Ignore = "Игнорировать";
			Reload = "Перезагрузка";
			Disable = "Выключить";
			Enable = "Включить";
			LoadData = "Загрузить данные";
			SaveData = "Сохранить данные";
			poll = "Результаты опроса: ";
			ChangeServer = "Сменить сервер";
			ChangePassword = "Сменить пароль";
			ChangeLogin = "Сменить логин";
			ChangeChannel = "Сменить канал";
			Cfg = "Конфигурация";
			Before = "Сохранить данные перед исполнением";
			LogFrame = "Лог";
			this.locale = locale;
			statuses = new String[] { "Неактивен", "Активен",
					"Активен - подгрузка данных, бот может не отвечать некоторое время", "Загрузка...",
					"Сохранение завершено", "Загрузка завершена", "Сохранение...", "Режим опроса активирован", "", "",
					"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
					"Список игнора обновлён", "Сменён канал, требуется перезапуск бота",
					"Сменён сервер, требуется перезапуск для входа на новый сервер.",
					"Сменён логин, требуется перезапуск бота для входа с новым логином",
					"Сменён пароль, требуется перезапуск бота для входа с новым паролем", "Завершение работы...",
					"Unknown", "Аутентификационные данные удалены, но вы пока можете использовать бота.",
					"Квантовый ключ перегенерирован, все данные перекодированы", "Сериализированные данные удалены!",
					"Не удалось подключиться к серверу!", "", "", "", "", "", "", "", "", "", "",
					"ВСЕ ДАННЫЕ УДАЛЕНЫ! Вы должны перезапустить бота для создания новых данных" };
		}
		if (locale.equalsIgnoreCase("EN")) {
			EnterLoginData = "Enter login data";
			Server = "Server:";
			Port = "Port:";
			Login = "Login:";
			Password = "Password:";
			Channel = "Channel:";
			SaveToFile = "Save to file";
			LoadFromFile = "Load from file";
			BLogin = "Log in";
			ToTray = "To tray";
			Exit = "Exit";
			BotLabel = "Status:";
			GenerateNew = "New quantum key";
			DeleteAll = "Delete ALL bot data";
			DeleteSerialized = "Delete serialized data";
			DeleteSaved = "Delete auth data";
			DangerZone = "Danger zone!";
			IKnow = "I know what i'm doing";
			Send = "Send";
			intervalsuffix2 = "min.";
			intervalsuffix = "sec.";
			Interval = "Interval:";
			Autosave = "Autosave";
			OpenLog = "Open log";
			Unignore = "Unignore";
			Ignore = "Ignore";
			Reload = "Reload";
			Disable = "Disable";
			Enable = "Enable";
			LoadData = "Load data";
			SaveData = "Save data";
			poll = "Poll results: ";
			ChangeServer = "Change server";
			ChangePassword = "Change password";
			ChangeLogin = "Change login";
			ChangeChannel = "Join to another channel";
			Cfg = "Config";
			Before = "Save data before executing";
			LogFrame = "Log";
			CloseLog = "Close log";
			this.locale = locale;
			statuses = new String[] { "Inactive", "Active",
					"Active - loading data, bot may be not responsive for some time", "Loading...", "Saved", "Loaded",
					"Saving...", "Polling mode activated", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "",
					"", "", "", "", "", "", "", "", "", "", "", "", "Ignore list updated",
					"Changed channel, bot restart required", "Changed server, bot restart required",
					"Changed login, bot restart required", "Changed password, bot restart required", "Disposing...",
					"Unknown", "Auth data removed, you still can use bot",
					"Quantum key regenerated, auth data reencrypted", "Serialized data deleted",
					"Failed to connect to the server!", "", "", "", "", "", "", "", "", "", "",
					"ALL DATA DELETED! You should restart bot to create new data" };
			return;
		}
	}
}
