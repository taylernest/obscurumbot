package com.refactoral.obscurumbot.core.struct;

import java.io.Serializable;

public class Config implements Serializable {
	public String SERVER = null;
	public String PORT = null;
	public String PASSWORD = null;
	public String CHAMBER = null;
	public String NAME = null;
}
