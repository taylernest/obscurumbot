package com.refactoral.obscurumbot.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import org.jibble.pircbot.PircBot;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.command.CLoad;
import com.refactoral.obscurumbot.command.CPoll;
import com.refactoral.obscurumbot.command.CSave;
import com.refactoral.obscurumbot.core.struct.Locale;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.struct.UserWallet;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.core.utils.MessageProvider;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;
import com.refactoral.obscurumbot.thread.BotTick;
import com.refactoral.obscurumbot.thread.SecondTick;

/**
 * ObscurumBot core. Cooldown, reloading, help command, listening for
 * messages...
 * 
 * @author GodusX
 */
public class ObscurumBot extends PircBot {
	public int delay;
	public int globalCooldown = 30;
	public String lastChannel;
	public String lastSender;
	public static ArrayList<String> messagesToDeliver = new ArrayList<String>();
	public static ArrayList<AbstractCommand> command = new ArrayList<AbstractCommand>();
	public static Map<String, UserWallet> uw = new HashMap<String, UserWallet>();
	private boolean active = false;
	public Mode mode = Mode.NORMAL;
	public boolean waiting = false;
	private int saveCounter = 0;
	public int queueSize = 0;
	boolean ignoreCooldown = false;
	Locale l = Main.locale;

	public void sendMessage(String msg) {
		sendMessage(lastChannel, msg);
		Log.info("ObscurumBot: " + msg);
	}

	public ObscurumBot() {
		super();
		StringCrypter c = new StringCrypter();
		delay = globalCooldown;
		this.setName(c.decrypt(Main.cfg.NAME));
		this.setLogin(c.decrypt(Main.cfg.NAME));
		mode = Mode.NORMAL;
		reload();
	}

	public void reload() {
		command.clear();
		Class<?>[] commands = BotHelper.getClasses();
		for (Class<?> c : commands) {
			if (c.getSuperclass().equals(AbstractCommand.class)) {
				AbstractCommand command;
				try {
					command = AbstractCommand.class.cast(c.newInstance());
					ObscurumBot.command.add(command);
				} catch (InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onMessage(String channel, String sender, String login, String hostname, String message) {
		if (sender.equalsIgnoreCase("jtv")) {
			twitchDeconstruct(message);
		}
		Log.info(sender + ": " + message);
		if (BotHelper.ignore.contains(sender)) {
			return;
		}
		if (uw.containsKey(sender)) {
			UserWallet w = uw.get(sender);
			if (w.counter > w.smalldelay) {
				w.counter = 0;
				w.cont = true;
			}
		} else {
			uw.put(sender, new UserWallet(sender));
		}
		MessageProvider.setItem(message, sender);
		ignoreCooldown = delay <= 0;
		for (String s : BotHelper.mods) {
			if (sender.toLowerCase().equals(s)) {
				ignoreCooldown = true;
				break;
			}
		}
		if (!ignoreCooldown) {
			return;
		}
		if (mode == Mode.POLL) {
			if (waiting) {
				if (message.startsWith("!варианты")) {
					boolean cont = false;
					for (short i = 0; i < BotHelper.mods.length; i++) {
						if (BotHelper.mods[i].equalsIgnoreCase(sender)) {
							cont = true;
							break;
						}
					}
					if (!cont) {
						return;
					}
					CPoll.results = new HashMap<String, Integer>();
					CPoll.voted = new ArrayList<String>();
					StringTokenizer st = new StringTokenizer(message);
					if (st.countTokens() < 3) {
						return;
					}
					String tmp, massstr = "";
					while (st.hasMoreTokens()) {
						tmp = st.nextToken();
						CPoll.results.put(tmp, 0);
						massstr += tmp + " | ";
					}
					sendMessage("Опрос создан! Тема: " + BotHelper.poll + ". Варианты ответа: " + massstr
							+ ". Введите !(ответ) в чат для голоса.");
				}
				waiting = false;
				return;
			}
			if (message.startsWith("!стоп")) {
				boolean cont = false;
				for (short i = 0; i < BotHelper.mods.length; i++) {
					if (BotHelper.mods[i].equalsIgnoreCase(sender)) {
						cont = true;
						break;
					}
				}
				if (!cont) {
					return;
				}
				BotHelper.poll = "";
				CPoll.results.forEach((key, element) -> {
					BotHelper.poll += key + ": " + element + " | ";
				});
				sendMessage("Опрос закончен: Результаты - " + BotHelper.poll.substring(11));
				mode = Mode.NORMAL;
				Main.frame.status.setText(l.poll + BotHelper.poll.substring(11));
				CPoll.results.clear();
				CPoll.voted.clear();
				CPoll.results = null;
				CPoll.voted = null;
				BotHelper.poll = null;

				return;
			}
			if (!CPoll.voted.contains(sender) && CPoll.results.containsKey(message.substring(1))) {
				CPoll.results.replace(message.substring(1), (CPoll.results.get(message.substring(1)) + 1));
				CPoll.voted.add(sender);
			}
			return;
		}
		for (AbstractCommand c : command) {
			if (c.trigger(message, sender, channel)) {
				c.performCommand(message, sender, channel);
			}
		}
		queueSize = messagesToDeliver.size();
		if (message.equalsIgnoreCase("!бот")) {
			ArrayList<String> commandsarray = new ArrayList<String>();
			for (AbstractCommand c : command) {
				if (c.getCommandTrigger() != null && !c.getCommandTrigger().isEmpty()) {
					commandsarray.add(c.getCommandTrigger());
				}
			}
			String sended = "Комманды: ";
			int cnt = 0;
			for (String s : commandsarray) {
				if (cnt == 0) {
					sended += " " + s;
				} else {
					sended += ", " + s;
				}
				++cnt;
			}
			this.sendMessage(channel, sended);
			commandsarray.clear();
			commandsarray.trimToSize();
			commandsarray = null;
		}
		if (messagesToDeliver.size() > queueSize && !ignoreCooldown) {
			delay = globalCooldown;
		}
	}

	private void twitchDeconstruct(String message) {
		StringTokenizer t = new StringTokenizer(message);
		if (t.countTokens() > 1) {
			if (t.nextToken().equalsIgnoreCase("SPECIALUSER")) {
				String usr = t.nextToken();
				sendMessage("@" + usr + " спасибо за подписку!");
			}
		}
	}

	public void mainLoop() {
		if (Main.frame.autosave.isSelected()) {
			if (saveCounter < Main.frame.slider.getValue()) {
				saveCounter++;
			} else {
				try {
					CSave.class.newInstance().performCommand("!save", "godusx", lastChannel);
				} catch (InstantiationException | IllegalAccessException e) {
					Log.error(e.getMessage());
					e.printStackTrace();
					return;
				}
				Log.info("Bot saved");
				Main.frame.status.setStatus(Opcodes.x05);
				saveCounter = 0;
			}
		} else if (saveCounter != 0) {
			saveCounter = 0;
		}
		uw.forEach((name, usr) -> {
			if (usr.counter < usr.delay) {
				usr.counter++;
			} else {
				if (usr.cont) {
					usr.cont = false;
				}
			}
			if (usr.smallcounter < usr.smalldelay && usr.cont) {
				usr.smallcounter++;
			} else if (usr.smallcounter >= usr.smalldelay && usr.cont) {
				usr.wallet++;
				usr.smallcounter = 0;
			} else if (!usr.cont && usr.smallcounter > 0) {
				usr.smallcounter = 0;
			}
		});
	}

	private int smallcntr = 0;

	public void secondLoop() {
		if (delay > 0) {
			--delay;
		}
		if (delay == 0) {
			if (!messagesToDeliver.isEmpty()) {
				sendMessage(messagesToDeliver.get(0));
				messagesToDeliver.remove(0);
				messagesToDeliver.trimToSize();
			}
		}
		if (smallcntr < 1200) {
			smallcntr++;
		} else {
			int rnd = 0 + (int) (Math.random() * ((2 - 0) + 1));
			String res = null;
			switch (rnd) {
			case 1:
				res = "Поддержи стримера! Qiwi: +79507123927 | Webmoney (RUB): R351050557985 | Webmoney: Z136050256664 | Paypal: good-people.oskol@yandex.ru | Yandex: 410013540236308";
				break;
			case 2:
				res = "Не забудь подписаться так же на канал на Youtube: https://www.youtube.com/user/CorporationMYTH";
				break;
			}
			if (res != null) {
				sendMessage(res);
			}
			smallcntr = 0;
		}
	}

	/**
	 * Start bot
	 */
	public void start() {
		Main.frame.frame.setEnabled(false);
		// Connecting to IRC server
		Log.info("Connecting to server");
		Main.frame.status.setStatus(Opcodes.x04);
		Main.frame.frame.repaint();
		StringCrypter c = new StringCrypter();
		try {
			CLoad.class.newInstance().performCommand("!load", "godusx", c.decrypt(Main.cfg.CHAMBER));
			Main.obot.connect(c.decrypt(Main.cfg.SERVER), Integer.parseInt(Main.cfg.PORT),
					c.decrypt(Main.cfg.PASSWORD));
			Main.obot.joinChannel(c.decrypt(Main.cfg.CHAMBER));
			if (c.decrypt(Main.cfg.SERVER).equalsIgnoreCase("irc.twitch.tv")) {
				this.sendRawLine("TWITCHCLIENT 2");
			}
		} catch (Exception e) {
			Log.error("Can't connect to server!");
			Log.error(e.getMessage());
			Main.frame.status.setStatus(Opcodes.xF4);
			Main.frame.frame.setEnabled(true);
			Main.frame.frame.repaint();
			c = null;
			return;
		}
		lastChannel = c.decrypt(Main.cfg.CHAMBER);
		c = null;
		BotTick.cont = true;
		SecondTick.cont = true;
		Log.info("Connection finished");
		Main.frame.frame.repaint();
		Main.frame.onEnabled();
		Main.tick.start();
		active = true;
		Main.stick.start();
		Main.frame.frame.setEnabled(true);
		Main.frame.status.setStatus(Opcodes.x02);
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(30000);
					Main.frame.status.setStatus(Opcodes.x01);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}.start();
		Main.frame.frame.repaint();
	}

	public void stop() {
		if (active) {
			BotTick.cont = false;
			SecondTick.cont = false;
			ObscurumBot.uw.clear();
			Main.frame.frame.setEnabled(false);
			Log.info("Disabling bot");
			Main.frame.status.setStatus(Opcodes.x00);
			Main.frame.frame.repaint();
			Main.obot.disconnect();
			Main.frame.frame.repaint();
			if (Main.obot.isConnected()) {
				Main.obot.dispose();
			}
			Main.frame.frame.repaint();
			Main.frame.onDisabled();
			Log.info("Bot disabled");
			active = false;
			Main.sleep(1000);
			Main.frame.frame.setEnabled(true);
			Main.frame.frame.repaint();
		}
	}

	public boolean isActive() {
		return active;
	}

	public void onLog(String message) {
		if (Main.frame.logframe.isVisible()) {
			Main.frame.logframe.logfield.setText(Main.frame.logframe.logfield.getText() + message + "\n");
		}
	}
}
