package com.refactoral.obscurumbot.core;

import java.io.File;

import com.refactoral.obscurumbot.core.utils.MessageProvider;

/**
 * Abstract command class.
 * Extend to add new command.
 * @author GodusX
 */
public abstract class AbstractCommand {
	/**
	 * Message provider
	 */
	protected MessageProvider mp = new MessageProvider();
	
	/**
	 * Gets alias for command.
	 * @return alias for command.
	 */
	public String[] getCommandAlias() {
		return new String[0];
	}
	/**
	 * Gets users that allowed to use command.
	 * @return string array with users.
	 * Override and return OBotHelper.mods if you creating administrative command
	 */
	public String[] getCommandUsers() {
		return new String[0];
	}
	/**
	 * Get command
	 * @return string that should be in message to execute command
	 */
	public abstract String getCommandTrigger();
	
	/**
	 * Code that will be executed on command running
	 * @param message - received message text
	 * @param sender - sender's nickname
	 * @param channel - channel from what message have been received
	 * @throws BotException if it needed
	 */
	public abstract void performCommand(String message, String sender, String channel);
	
	/**
	 * Run code on data saving
	 * @param dir - directory to save
	 */
	public void onDataSave(File dir) { }
	/**
	 * Run code on data loading
	 * @param dir - directory to save
	 */
	public void onDataLoad(File dir) { }
	/**
	 * Run code on command tick
	 * Not recommended to use
	 */
	public void onCommandTick() { }
	/**
	 * Run code on command unregistering
	 * Not sure is this actually can be used and works
	 */
	public void onCommandUnregister() { }
	/**
	 * Run code on command registering
	 * Looks useful
	 */
	public void onCommandRegistered() { }
	
	/**
	 * Checks if getCommandTrigger() or one of alias equals message<br>
	 * You can override this method, so while overriding you can simply return "true" and make class-container for commands and check some things by yourself
	 * Actually such thing simply let you do some code on message receiving
	 * Otherwise, you can return "false" to prevent command usage
	 * @param command
	 * @param sender
	 * @param channel
	 * @return true to let code be executed
	 */
	public boolean trigger(String command, String sender, String channel) {
		if (getCommandUsers().length > 0)
			if (!arrayStringEquals(getCommandUsers(), sender)) {
				return false;
			}
		return command.toLowerCase().startsWith(getCommandTrigger()) || arrayStringEquals(getCommandAlias(), command);
	}
	
	/**
	 * Iterates array and checks for first element that equals to compared string
	 * @param array - string array
	 * @param compared - compared string
	 * @return true if array have equal string
	 */
	public boolean arrayStringEquals(String[] array, String compared) {
		for (String s : array) {
			if (s.toLowerCase().equals(compared)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean arrayStringContains(String[] array, String compared) {
		for (String s : array) {
			if (s.toLowerCase().contains(compared)) {
				return true;
			}
		}
		return false;
	}
}
