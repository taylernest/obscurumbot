package com.refactoral.obscurumbot.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.gui.component.LogArea;
import java.awt.Font;
import javax.swing.ScrollPaneConstants;

public class LogFrame extends JFrame {
	public LogArea logfield;
	public LogFrame() {
		super(Main.locale.LogFrame);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		logfield = new LogArea();
		logfield.setFont(new Font("Monospaced", Font.PLAIN, 12));
		scrollPane.setViewportView(logfield);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(new Dimension(300, 500));
		this.setLocationRelativeTo(null);
		logfield.setLineWrap(true);
	}
}
