package com.refactoral.obscurumbot.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;

public class InputFrame extends JFrame {

	/**
	 * Description.
	 */
	private static final long serialVersionUID = -7958043629492127463L;
	private JTextField input;
	private JButton ok;
	private JFrame frame;
	public InputAction action;

	/**
	 * Create the frame.
	 */
	public InputFrame() {
		frame = this;
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 310, 130);
		input = new JTextField(16);
		input.setBounds(10, 11, 282, 20);
		ok = new JButton("OK");
		ok.setBounds(84, 69, 144, 23);
		getContentPane().setLayout(null);
		this.getContentPane().add(input);
		this.getContentPane().add(ok);
		this.setAlwaysOnTop(true);
		
		JCheckBox savedata = new JCheckBox(Main.locale.Before);
		savedata.setBounds(10, 38, 135, 23);
		getContentPane().add(savedata);
		savedata.setVisible(false);
		ok.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (input.getText() != "" && !input.getText().contains(" ") && input.getText().length() > 3) {
					if (action == InputAction.IGNORE) {
						Log.info("Adding new entry to ignore list: " + input.getText());
						BotHelper.ignore.add(input.getText());
						Main.frame.status.setStatus(Opcodes.x35);
					} else if (action == InputAction.UNIGNORE) {
						Log.info("Removing entry from ignore list: " + input.getText());
						BotHelper.ignore.remove(input.getText());
						Main.frame.status.setStatus(Opcodes.x35);
					} else if (action == InputAction.CHANNEL) {
						Main.cfg.CHAMBER = new StringCrypter().encrypt(input.getText());
						Log.info("Changed channel");
						Main.frame.status.setStatus(Opcodes.x36);
					} else if (action == InputAction.LOGIN) {
						Main.cfg.NAME = new StringCrypter().encrypt(input.getText());
						Log.info("Changed login");
						Main.frame.status.setStatus(Opcodes.x38);
					} else if (action == InputAction.SERVER) {
						Main.cfg.SERVER = new StringCrypter().encrypt(input.getText());
						Log.info("Changed server");
						Main.frame.status.setStatus(Opcodes.x37);
					} else if (action == InputAction.PASSWORD) {
						Main.cfg.PASSWORD = new StringCrypter().encrypt(input.getText());
						Log.info("Changed password");
						Main.frame.status.setStatus(Opcodes.x39);
					}
				}
				Main.frame.frame.setEnabled(true);
				frame.setVisible(false);
				input.setText("");
			}
		});
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		setVisible(false);
	}

	public void destroy() {
		this.getContentPane().removeAll();
		input = null;
		ok = null;
		frame = null;
	}
}
