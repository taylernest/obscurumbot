package com.refactoral.obscurumbot.gui;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.command.CLoad;
import com.refactoral.obscurumbot.command.CSave;
import com.refactoral.obscurumbot.core.struct.Key;
import com.refactoral.obscurumbot.core.struct.Locale;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.core.utils.crypto.QuantumRandom;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;
import com.refactoral.obscurumbot.gui.component.CodedLabel;
import com.refactoral.obscurumbot.gui.component.MotionPanel;
import com.refactoral.obscurumbot.thread.BotTick;
import com.refactoral.obscurumbot.thread.SecondTick;

public class MainWindow {

	public JFrame frame;
	public JLabel bot;
	public CodedLabel status;
	public JButton start, stop, reboot, exit, load, save, ignore, unignore, btnJoinToAnother;
	public Object caller;
	public InputFrame input;
	public TrayIcon trayIcon;
	public SystemTray tray;
	public JSlider slider;
	public JCheckBox autosave;
	public JLabel lblInterval;
	public JTextArea txtrMessage;
	public JButton btnSend;
	public JLabel interval;
	private JTabbedPane tabbedPane;
	private JPanel panel;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnClearAllBot;
	private JButton chatlogopen;
	private Locale l;
	private JButton changeserver;
	private JButton changeauth;
	private JButton btnNewButton_3;
	public LogFrame logframe;

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		input = new InputFrame();
		l = Main.locale;
		frame = new JFrame("ObscurumBot");
		logframe = new LogFrame();
		MotionPanel motionPanel = new MotionPanel(frame);
		frame.setContentPane(motionPanel);
		frame.setUndecorated(true);
		frame.setResizable(false);
		Main.frame = this;
		Main.con.removeAll();
		Main.con.channel = null;
		Main.con.login = null;
		Main.con.password = null;
		Main.con.port = null;
		Main.con.servip = null;
		Main.con.dispose();
		Main.con = null;
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Log.info("Loading main window");
		frame.setBounds(100, 100, 500, 549);
		frame.getContentPane().setLayout(null);
		bot = new JLabel(l.BotLabel);
		bot.setBounds(10, 38, 52, 14);
		frame.getContentPane().add(bot);
		status = new CodedLabel();
		status.setBounds(60, 38, 430, 14);
		frame.getContentPane().add(status);
		start = new JButton(l.Enable);
		start.setBounds(10, 63, 98, 23);
		frame.getContentPane().add(start);
		stop = new JButton(l.Disable);
		stop.setBounds(386, 63, 98, 23);
		frame.getContentPane().add(stop);
		stop.setEnabled(false);
		reboot = new JButton(l.Reload);
		reboot.setBounds(175, 63, 149, 23);
		frame.getContentPane().add(reboot);
		reboot.setEnabled(false);
		load = new JButton(l.LoadData);
		load.setBounds(353, 131, 131, 23);
		frame.getContentPane().add(load);
		load.setEnabled(false);
		save = new JButton(l.SaveData);
		save.setBounds(353, 97, 131, 23);
		frame.getContentPane().add(save);
		save.setEnabled(false);
		ignore = new JButton(l.Ignore);
		ignore.setEnabled(false);
		ignore.setBounds(10, 97, 131, 23);
		frame.getContentPane().add(ignore);
		unignore = new JButton(l.Unignore);
		unignore.setEnabled(false);
		unignore.setBounds(10, 131, 131, 23);
		frame.getContentPane().add(unignore);
		interval = new JLabel("15 " + l.intervalsuffix2 + " ");
		interval.setBounds(200, 165, 284, 14);
		frame.getContentPane().add(interval);

		slider = new JSlider();
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				interval.setText(slider.getValue() + " " + l.intervalsuffix + " ("
						+ (float) ((float) slider.getValue() / 60.0F) + " " + l.intervalsuffix2 + ")");
			}
		});
		slider.setMinimum(60);
		slider.setMaximum(1800);
		slider.setValue(900);
		slider.setBounds(10, 190, 474, 24);
		frame.getContentPane().add(slider);

		autosave = new JCheckBox(l.Autosave);
		autosave.setBounds(10, 161, 110, 23);
		frame.getContentPane().add(autosave);

		lblInterval = new JLabel(l.Interval);
		lblInterval.setBounds(126, 165, 70, 14);
		frame.getContentPane().add(lblInterval);

		btnSend = new JButton(l.Send);
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.obot.sendMessage(txtrMessage.getText());
				txtrMessage.setText("");
			}
		});

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 225, 474, 66);
		frame.getContentPane().add(scrollPane);

		txtrMessage = new JTextArea();
		scrollPane.setViewportView(txtrMessage);
		txtrMessage.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					arg0.consume();
					if (btnSend.isEnabled()) {
						btnSend.doClick();
					}
				}
			}
		});
		txtrMessage.setFont(new Font("Monospaced", Font.PLAIN, 12));
		btnSend.setEnabled(false);
		btnSend.setBounds(386, 297, 98, 23);
		frame.getContentPane().add(btnSend);

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setEnabled(false);
		tabbedPane.setBounds(10, 346, 311, 192);
		frame.getContentPane().add(tabbedPane);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab(l.Cfg, null, panel_1, null);
		panel_1.setLayout(null);

		changeserver = new JButton(l.ChangeServer);
		changeserver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setEnabled(false);
				input.action = InputAction.SERVER;
				input.setVisible(true);
			}
		});
		changeserver.setEnabled(false);
		changeserver.setBounds(10, 11, 284, 23);
		panel_1.add(changeserver);

		changeauth = new JButton(l.ChangeLogin);
		changeauth.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setEnabled(false);
				input.action = InputAction.LOGIN;
				input.setVisible(true);
			}
		});
		changeauth.setEnabled(false);
		changeauth.setBounds(10, 45, 284, 23);
		panel_1.add(changeauth);

		btnJoinToAnother = new JButton(l.ChangeChannel);
		btnJoinToAnother.setBounds(10, 113, 284, 23);
		panel_1.add(btnJoinToAnother);
		btnJoinToAnother.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setEnabled(false);
				input.action = InputAction.CHANNEL;
				input.setVisible(true);
			}
		});
		btnJoinToAnother.setEnabled(false);
		
		btnNewButton_3 = new JButton(l.ChangePassword);
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setEnabled(false);
				input.action = InputAction.PASSWORD;
				input.setVisible(true);
			}
		});
		btnNewButton_3.setEnabled(false);
		btnNewButton_3.setBounds(10, 79, 284, 23);
		panel_1.add(btnNewButton_3);

		panel = new JPanel();
		panel.setBackground(new Color(255, 215, 0));
		tabbedPane.addTab(l.DangerZone, null, panel, null);
		panel.setLayout(null);

		btnNewButton = new JButton(l.DeleteSaved);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File f = new File("data/auth.dat");
				f.delete();
				status.setStatus(Opcodes.xF1);
			}
		});
		btnNewButton.setBackground(new Color(244, 164, 96));
		btnNewButton.setEnabled(false);
		btnNewButton.setBounds(10, 11, 285, 23);
		panel.add(btnNewButton);

		btnNewButton_1 = new JButton(l.GenerateNew);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					StringCrypter s = new StringCrypter();
					String name = s.decrypt(Main.cfg.NAME);
					String pass = s.decrypt(Main.cfg.PASSWORD);
					String server = s.decrypt(Main.cfg.SERVER);
					String chamber = s.decrypt(Main.cfg.CHAMBER);
					s = null;
					File f;
					File ign = new File("data/comm/Ignore-" + Main.cfg.CHAMBER + ".dat");
					File szd = new File("data/serialized/" + Main.cfg.CHAMBER + ".dat");
					File szo = new File("data/serialized/" + Main.cfg.CHAMBER + ".szo");
					f = new File("data/key.dat");
					f.delete();
					f.createNewFile();
					Key k = new Key();
					k.key = new QuantumRandom(8).getBytes();
					ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f));
					oos.writeObject(k);
					oos.close();
					oos = null;
					k.key = null;
					k = null;
					s = new StringCrypter();
					Main.cfg.PASSWORD = s.encrypt(pass);
					pass = null;
					Main.cfg.CHAMBER = s.encrypt(chamber);
					chamber = null;
					Main.cfg.NAME = s.encrypt(name);
					name = null;
					Main.cfg.SERVER = s.encrypt(server);
					server = null;
					s = null;
					if (ign.exists()) {
						ign.renameTo(new File("data/comm/Ignore-" + Main.cfg.CHAMBER + ".dat"));
					}
					if (szd.exists()) {
						szd.renameTo(new File("data/serialized/" + Main.cfg.CHAMBER + ".dat"));
					}
					if (szo.exists()) {
						szo.renameTo(new File("data/serialized/" + Main.cfg.CHAMBER + ".szo"));
					}
					f = new File("data/auth.dat");
					if (f.exists()) {
						f.delete();
					}
					f.createNewFile();
					oos = new ObjectOutputStream(new FileOutputStream(f));
					oos.writeObject(Main.cfg);
					oos.close();
					oos = null;
					f = null;
					status.setStatus(Opcodes.xF2);
				} catch (Exception eeee) {
					return;
				}
			}
		});
		btnNewButton_1.setBackground(new Color(244, 164, 96));
		btnNewButton_1.setEnabled(false);
		btnNewButton_1.setBounds(10, 45, 285, 23);
		panel.add(btnNewButton_1);

		btnNewButton_2 = new JButton(l.DeleteSerialized);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.obot.stop();
				File f = new File("data/serialized");
				BotHelper.delete(f);
				f = null;
				status.setStatus(Opcodes.xF3);
			}
		});
		btnNewButton_2.setBackground(new Color(244, 164, 96));
		btnNewButton_2.setEnabled(false);
		btnNewButton_2.setBounds(10, 79, 285, 23);
		panel.add(btnNewButton_2);
		JCheckBox rusure = new JCheckBox(l.IKnow);

		btnClearAllBot = new JButton(l.DeleteAll);
		btnClearAllBot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.obot.stop();
				BotTick.cont = false;
				SecondTick.cont = false;
				Main.tick = null;
				Main.stick = null;
				Main.cfg = null;
				BotHelper.delete(new File("data"));
				rusure.doClick();
				rusure.setEnabled(false);
				status.setStatus(Opcodes.xFF);
				System.gc();
				start.setEnabled(false);
			}
		});
		btnClearAllBot.setBackground(new Color(244, 164, 96));
		btnClearAllBot.setEnabled(false);
		btnClearAllBot.setBounds(10, 113, 285, 23);
		panel.add(btnClearAllBot);

		rusure.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rusure.isSelected()) {
					tabbedPane.setEnabled(true);
					btnNewButton.setEnabled(true);
					btnNewButton_1.setEnabled(true);
					btnNewButton_2.setEnabled(true);
					btnClearAllBot.setEnabled(true);
					btnJoinToAnother.setEnabled(true);
					changeserver.setEnabled(true);
					changeauth.setEnabled(true);
					btnNewButton_3.setEnabled(true);
				} else {
					tabbedPane.setEnabled(false);
					btnNewButton.setEnabled(false);
					btnNewButton_1.setEnabled(false);
					btnNewButton_2.setEnabled(false);
					btnClearAllBot.setEnabled(false);
					btnJoinToAnother.setEnabled(false);
					changeserver.setEnabled(false);
					changeauth.setEnabled(false);
					btnNewButton_3.setEnabled(false);
				}
			}
		});
		rusure.setBounds(11, 316, 224, 23);
		frame.getContentPane().add(rusure);

		chatlogopen = new JButton(l.OpenLog);
		chatlogopen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!logframe.isVisible()) {
					logframe.setVisible(true);
					chatlogopen.setText(l.CloseLog);
				} else {
					chatlogopen.setText(l.OpenLog);
					logframe.setVisible(false);
					logframe.logfield.setText("");
				}
			}
		});
		chatlogopen.setBounds(175, 131, 149, 23);
		frame.getContentPane().add(chatlogopen);

		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(10, 11, 149, 23);
		frame.getContentPane().add(toolBar);
		toolBar.setFloatable(false);

		JLabel lblObscurumbot = new JLabel("ObscurumBot");
		lblObscurumbot.setFont(new Font("Segoe UI Symbol", Font.BOLD, 18));
		toolBar.add(lblObscurumbot);
		exit = new JButton(l.Exit);
		exit.setBounds(405, 11, 79, 23);
		frame.getContentPane().add(exit);

		JButton btnToTray = new JButton(l.ToTray);
		btnToTray.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					tray.add(trayIcon);
					frame.setState(7);
					frame.setVisible(false);
				} catch (AWTException e1) {
				}
			}
		});
		btnToTray.setBounds(304, 11, 91, 23);
		frame.getContentPane().add(btnToTray);
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.doExit();
			}
		});

		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.obot.start();
			}
		});
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.obot.stop();
			}
		});
		reboot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Log.info("Shutting down bot and cleaning up memory");
				start.setEnabled(false);
				stop.setEnabled(false);
				frame.setEnabled(false);
				Main.obot.stop();
				Main.obot.start();
				frame.setEnabled(true);
				stop.setEnabled(true);
			}
		});
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					CLoad.class.newInstance().performCommand(null, null, Main.obot.lastChannel);
				} catch (Exception e) {
					Log.error(e.getMessage());
				}
			}
		});
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					CSave.class.newInstance().performCommand(null, null, Main.obot.lastChannel);
				} catch (Exception e) {
					Log.error(e.getMessage());
				}
			}
		});
		ignore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setEnabled(false);
				input.action = InputAction.IGNORE;
				input.setVisible(true);
			}
		});
		unignore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setEnabled(false);
				input.action = InputAction.UNIGNORE;
				input.setVisible(true);
			}
		});
		if (SystemTray.isSupported()) {
			Log.info("Tray supported");
			tray = SystemTray.getSystemTray();
			Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png"));
			trayIcon = new TrayIcon(image, "ObscurumBot");
			trayIcon.setImageAutoSize(true);
			trayIcon.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent e) {
					if (e.getClickCount() == 1) {
						frame.setState(JFrame.NORMAL);
						frame.setVisible(true);
						tray.remove(trayIcon);
						frame.repaint();
						frame.toFront();
					}
				}
			});
		} else {
			btnToTray.setVisible(false);
			Log.warn("System tray not supported");
		}
		frame.addWindowStateListener(wsl -> {
			if (wsl.getNewState() == JFrame.ICONIFIED) {
				try {
					tray.add(trayIcon);
					frame.setVisible(false);
				} catch (AWTException ex) {
					Log.error("Unable to add tray icon");
				}
			}
			if (wsl.getNewState() == 7) {
				try {
					tray.add(trayIcon);
					frame.setVisible(false);
				} catch (AWTException ex) {
					Log.error("Unable to add tray icon");
				}
			}
			if (wsl.getNewState() == JFrame.MAXIMIZED_BOTH) {
				tray.remove(trayIcon);
				frame.setVisible(true);
			}
			if (wsl.getNewState() == JFrame.NORMAL) {
				tray.remove(trayIcon);
				frame.setVisible(true);
			}
		});
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));
		frame.setLocationRelativeTo(null);
		onDisabled();
		frame.setVisible(true);
		frame.toFront();
	}

	/**
	 * Used to destroy elements
	 */
	public void destroy() {
		frame.getContentPane().removeAll();
		bot = null;
		status = null;
		start = null;
		stop = null;
		reboot = null;
		exit = null;
		load = null;
		save = null;
		ignore = null;
		unignore = null;
		caller = null;
		trayIcon = null;
		tray = null;
		slider = null;
		autosave = null;
		lblInterval = null;
		txtrMessage = null;
		btnSend = null;
		interval = null;
		tabbedPane = null;
		panel = null;
		btnNewButton = null;
		btnNewButton_1 = null;
		btnNewButton_2 = null;
		btnClearAllBot = null;
		chatlogopen = null;
		l = null;
		changeserver = null;
		changeauth = null;
		btnNewButton_3 = null;
		logframe.setVisible(false);
		logframe.getContentPane().removeAll();
		logframe.removeAll();
		logframe.logfield = null;
		logframe.dispose();
		logframe = null;
		frame.removeAll();
		frame.dispose();
		input.removeAll();
		input.destroy();
		input.dispose();
		input = null;
	}

	/**
	 * Set GUI state to enabled Start method call this method Not recommended
	 * for use except you know what you doing
	 */
	public void onEnabled() {
		start.setEnabled(false);
		stop.setEnabled(true);
		reboot.setEnabled(true);
		load.setEnabled(true);
		ignore.setEnabled(true);
		unignore.setEnabled(true);
		save.setEnabled(true);
		btnSend.setEnabled(true);
		autosave.setEnabled(true);
		slider.setEnabled(true);
	}

	/**
	 * Set GUI state to disabled Stop method calls this method Not recommended
	 * for use except you know what you doing
	 */
	public void onDisabled() {
		stop.setEnabled(false);
		load.setEnabled(false);
		save.setEnabled(false);
		reboot.setEnabled(false);
		ignore.setEnabled(false);
		unignore.setEnabled(false);
		start.setEnabled(true);
		btnSend.setEnabled(false);
		if (autosave.isSelected()) {
			autosave.setSelected(false);
		}
		slider.setEnabled(false);
		autosave.setEnabled(false);
	}
}
