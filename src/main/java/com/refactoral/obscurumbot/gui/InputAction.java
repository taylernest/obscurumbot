package com.refactoral.obscurumbot.gui;

public enum InputAction {
	NONE, CHANNEL, IGNORE, UNIGNORE, LOGIN, SERVER, PASSWORD
}
