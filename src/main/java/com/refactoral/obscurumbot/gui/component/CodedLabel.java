package com.refactoral.obscurumbot.gui.component;

import javax.swing.JLabel;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.struct.Opcodes;

public class CodedLabel extends JLabel {
	private Opcodes status = Opcodes.x00;

	public void setStatus(Opcodes code) {
		setText(Main.locale.statuses[code.ordinal()] + " (0" + code.name() + ")");
		status = code;
	}

	public Opcodes getStatus() {
		return status;
	}

	public CodedLabel() {
		super("N/A");
	}
}
