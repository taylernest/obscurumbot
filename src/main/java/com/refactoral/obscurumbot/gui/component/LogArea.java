package com.refactoral.obscurumbot.gui.component;

import javax.swing.JTextArea;

public class LogArea extends JTextArea {
	
	public LogArea() {
		super("");
		this.setEditable(false);
	}
	
}
