package com.refactoral.obscurumbot.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.struct.Config;
import com.refactoral.obscurumbot.core.struct.Locale;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;
import com.refactoral.obscurumbot.thread.BotTick;
import com.refactoral.obscurumbot.thread.SecondTick;

public class ConnectionFrame extends JFrame {

	/**
	 * Description.
	 */
	private static final long serialVersionUID = -7958043629492127463L;
	File dat;
	JTextField servip;
	JTextField port;
	JTextField login;
	JTextField password;
	JTextField channel;
	JButton btnLogIn;
	public JButton btnLoadData;
	File f;

	/**
	 * Create the frame.
	 */
	public ConnectionFrame() {
		f = new File("data");
		if (f.exists()) {
			f = new File("data/lang.dat");
			if (f.exists()) {
				try {
					BufferedReader fbr = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
					Main.locale = new Locale(fbr.readLine());
					fbr.close();
					fbr = null;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		} else {
			f.mkdirs();
			f = new File("data/lang.dat");
		}
		Locale l = Main.locale;
		dat = new File("data/auth.dat");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		JLabel lblEnterLoginData = new JLabel(l.EnterLoginData);
		lblEnterLoginData.setHorizontalAlignment(SwingConstants.CENTER);
		lblEnterLoginData.setBounds(84, 11, 208, 14);
		getContentPane().add(lblEnterLoginData);

		btnLogIn = new JButton(l.BLogin);
		btnLogIn.setBounds(10, 158, 97, 23);
		getContentPane().add(btnLogIn);

		btnLoadData = new JButton(l.LoadData);
		btnLoadData.setToolTipText(l.LoadFromFile);
		btnLoadData.setBounds(212, 158, 145, 23);
		getContentPane().add(btnLoadData);

		JLabel lblServer = new JLabel(l.Server);
		lblServer.setBounds(10, 35, 46, 14);
		getContentPane().add(lblServer);

		servip = new JTextField();
		servip.setBounds(66, 32, 291, 20);
		getContentPane().add(servip);
		servip.setColumns(10);

		JLabel lblPort = new JLabel(l.Port);
		lblPort.setBounds(10, 60, 46, 14);
		getContentPane().add(lblPort);

		port = new JTextField();
		port.setBounds(66, 57, 86, 20);
		getContentPane().add(port);
		port.setColumns(10);

		JLabel lblLogin = new JLabel(l.Login);
		lblLogin.setBounds(162, 60, 40, 14);
		getContentPane().add(lblLogin);

		login = new JTextField();
		login.setBounds(212, 57, 145, 20);
		getContentPane().add(login);
		login.setColumns(10);

		JLabel lblPassword = new JLabel(l.Password);
		lblPassword.setBounds(10, 85, 60, 14);
		getContentPane().add(lblPassword);

		password = new JTextField();
		password.setBounds(66, 82, 291, 20);
		getContentPane().add(password);
		password.setColumns(10);

		JLabel lblChannel = new JLabel(l.Channel);
		lblChannel.setBounds(10, 110, 52, 14);
		getContentPane().add(lblChannel);

		channel = new JTextField();
		channel.setBounds(66, 107, 291, 20);
		getContentPane().add(channel);
		channel.setColumns(10);

		JCheckBox chckbxSaveToFile = new JCheckBox(l.SaveToFile);
		chckbxSaveToFile.setBounds(10, 131, 142, 23);
		getContentPane().add(chckbxSaveToFile);
		setVisible(false);
		btnLoadData.setEnabled(false);

		JCheckBox rus = new JCheckBox("Russian");
		rus.setToolTipText("New language will be applied to this window after restart");
		rus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (rus.isSelected()) {
					Main.locale = new Locale("RU");
				} else {
					Main.locale = new Locale("EN");
				}
			}
		});
		rus.setBounds(117, 158, 79, 23);
		getContentPane().add(rus);
		if (dat.exists()) {
			if (!dat.isDirectory()) {
				btnLoadData.setEnabled(true);
			} else {
				dat.delete();
			}
		}
		if(Main.locale.locale.equalsIgnoreCase("RU")) {
			rus.doClick();
		}
		btnLogIn.addActionListener(action -> {
			this.setVisible(false);
			StringCrypter c = new StringCrypter();
			Main.cfg = new Config();
			Main.cfg.NAME = c.encrypt(login.getText());
			Main.cfg.CHAMBER = c.encrypt(channel.getText());
			Main.cfg.PASSWORD = c.encrypt(password.getText());
			Main.cfg.PORT = port.getText();
			Main.cfg.SERVER = c.encrypt(servip.getText());
			c = null;
			if (chckbxSaveToFile.isSelected()) {
				write();
			}
			finish();
		});
		btnLoadData.addActionListener(action -> {
			try {
				FileInputStream fi = new FileInputStream(dat);
				ObjectInputStream oi = new ObjectInputStream(fi);
				Main.cfg = (Config) oi.readObject();
				oi.close();
				fi.close();
				fi = null;
				oi = null;
				finish();
			} catch (Exception e1) {
				e1.printStackTrace();
				System.exit(1);
			}
		});
		this.setSize(375, 220);
		this.setResizable(false);
	}

	private void finish() {
		Main.stick = new SecondTick();
		Main.tick = new BotTick();
		Main.obot = new ObscurumBot();
		getContentPane().removeAll();
		dat = null;
		new MainWindow();
		try {
			if (f.exists()) {
				f.delete();
			}
			if (!f.exists()) {
				f.createNewFile();
			}
			FileOutputStream oos = new FileOutputStream(f);
			oos.write(Main.locale.locale.getBytes());
			oos.close();
			oos = null;
			f = null;
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void write() {
		try {
			if (dat.exists()) {
				dat.delete();
			}
			if (!dat.exists()) {
				dat.createNewFile();
			}
			FileOutputStream fo = new FileOutputStream(dat);
			ObjectOutputStream oo = new ObjectOutputStream(fo);
			oo.writeObject(Main.cfg);
			oo.flush();
			oo.close();
			fo.close();
			oo = null;
			fo = null;
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
