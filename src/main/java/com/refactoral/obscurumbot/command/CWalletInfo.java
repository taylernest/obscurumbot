package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;

public class CWalletInfo extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!валюта";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		mp.Send("Здесь ваша валюта - Попкорн. Получать вы его можете не просто сидя на стриме, а общаясь с другими зрителями или стримером. Каждые 15 минут вы получаете 1 попкорн если в промежутке между получениями попкорна вы хоть раз общались. Минимальный интервал между сообщениями для продолжения получения попкорна - 5 минут");
	}

}
