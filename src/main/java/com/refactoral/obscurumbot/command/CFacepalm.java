package com.refactoral.obscurumbot.command;

import java.util.Random;

import com.refactoral.obscurumbot.core.AbstractCommand;

/**
 * BREAK ALL THE THINGS! BALLS OF STEEL!
 * @author GodusX
 *
 */
public final class CFacepalm extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!facepalm";
	}

	public String[] getCommandAlias() {
		return new String[] { "!лицо", "!рукаглаза", "!руказатылок", "!рукалицо", "!ногалицо" };
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		String[] items = new String[] { "бутылку", "чашку чая", "микрофон", "клавиатуру", "монитор", "мышь", "стол",
				"тумбочку", "часы", "книгу", "книжный шкаф", "тарелку", "пиццу", "чатик", "Годуса", "кровать", "дверь",
				"дом", "стул", "вентилятор", "печеньки", "Лавенду", "себя", "Вовуноксина", "себя", "Мубота", "�?нтернет",
				"твитч", "окно", "голову", "чайник", "мыло", "компьютер", "шкаф с одеждой", "всю вселенную", "мудреца",
				"код", "матрицу", "T-1000", "все 3 клинка", "собрание сочинений толкиена", "капитана очевидность",
				"телевизор", "антарктиду", "кусок льда", "коллайдер", "злую собаку", "поезд", "63 автобус", "самолёт",
				"чайник", "обувной магазин", "ботинок 64 размера", "ArrayIndexOutOfBoundsException",
				"NullPointerException", "AriphmeticException", "вечность", "фейспалм", "эту комманду", "тысячу чертей",
				"якорь", "Валуева", "Боярского", "силушку богатырскую", "древнюю русь", "говорящую лошадь",
				"мировой запас золота", "Титаник", "крейсер", "дядю Стёпу", "лурк", "яойный фанфик", "юрийный фанфик",
				"рысей", "ведро Свазки", "бутылку Свазки", "Вазки со Свазки" };

		Random rnd = new Random(System.currentTimeMillis());

		this.mp.Send("/me разбил " + items[rnd.nextInt(items.length)] + " о голову " + sender);
	}
}
