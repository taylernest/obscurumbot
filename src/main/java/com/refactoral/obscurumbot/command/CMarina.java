package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;

public class CMarina extends AbstractCommand {
	@Override
	public String getCommandTrigger() {
		return "!марина";
	}
	@Override
	public void performCommand(String message, String sender, String channel) {
		mp.Send("Всем здрасти. Меня зовут Марина. Мне 32 года, живу в Радужном (который в ХМАО), люблю песочницы, квестики, стратегии и т.п. =)");
	}
}