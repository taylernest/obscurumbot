package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;

/**
 * Meow! No!
 * @author GodusX
 *
 */
public final class CCat extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!ня";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		this.mp.Send("A cat is fine too. Desu~ Desu~ Desu~");
	}
}
