package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;

/**
 * Changes chamber
 * @author GodusX
 *
 */
public final class CGoto extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "!goto";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		try {
			String channelStr = message.substring(9);
			this.mp.Send("Хорошо, хозяин...");
			Main.obot.partChannel(channel, "Я ушель к другому...");
			Main.obot.joinChannel("#" + channelStr);
			Main.obot.lastChannel = "#" + channelStr;
		} catch (Exception e) {
		}
	}
}
