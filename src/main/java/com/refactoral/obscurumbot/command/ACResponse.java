package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.MessageProvider;

/**
 * Abstract command class
 * Random commands and easter eggs
 * @author GodusX
 *
 */
public final class ACResponse extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return null;
	}

	public boolean trigger(String command, String sender, String channel) {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		MessageProvider.setItem(message, sender);
		if (message.toLowerCase().contains("какие такие ошибки?") && sender.equalsIgnoreCase("godusx")) {
			this.mp.Send("В коде, дурак!");
		}
		if (message.toLowerCase().contains("это утверждение ложно")) {
			this.mp.Send("LogicalException $ Paradox");
		}
		if (message.toLowerCase().contains("в недрах тундры выдра в гетрах тырит в ведрах ядра кедра") || message
				.toLowerCase().contains("саша машет паше, паша машет саше. саша краше маши, маша краше паши")) {
			this.mp.Send("Карл у Клары украл рекламу, а Клара у Карла украла бюджет");
		}
		if (message.toLowerCase().contains("я вернутый")) {
			this.mp.Send("С развратом");
		}
		if (message.toLowerCase().equals("зачем") || message.toLowerCase().equals("gjxtve")) {
			this.mp.Send("Потому что гладиолус.");
		}
		if (message.toLowerCase().contains("торт - ложь") || message.toLowerCase().contains("the cake is a lie")) {
			this.mp.Send("deIlluminati");
		}
		if (message.toLowerCase().contains("бекон")) {
			this.mp.Send("Был час ночи, горела свиноферма...");
		}
		if (message.toLowerCase().contains("разведка")) {
			this.mp.Send(" В руках граната, в попе ветка - это к нам ползёт разведка!");
		}
		if (message.toLowerCase().contains("землю видно")) {
			this.mp.Send("Земля в иллюминаторе! Земля в иллюминаторе!..");
		}
		if (message.toLowerCase().contains("я пошёл кушать")) {
			this.mp.Send("Приятного апетита!");
		}
		if ((message.toLowerCase().contains("я пойду") || message.toLowerCase().contains("я пошел")
				|| message.toLowerCase().contains("пойду я")) && sender.equalsIgnoreCase("godusx")) {
			this.mp.Send("Пока, создатель :(");
		}
		if ((message.toLowerCase().contains("бот остаётся тут") && sender.equalsIgnoreCase("godusx"))) {
			this.mp.Send("Эй!");
		}
		if (message.contains("/0") || message.contains("/ 0")) {
			this.mp.Send("LogicalException $ Divided by 0");
		}
		if (this.mp.contains("null")) {
			this.mp.Send("java.lang.NullPointerException");
		}
		if (this.mp.contains("Каков смысл жизни, вселенной и всего сущего?")) {
			this.mp.Send("42");
		} 
		if (this.mp.contains("клара освин освальд")) {
			this.mp.Send("Хрю-хрю!");
		}
		/*if (this.mp.contains(
				"Как измерить тангенциально - касательное ускорение пингвина запущенного в пирамидальный интеграл со скоростью вращательного косинуса в системе злополучных носков?")) {
			this.mp.Send(
					"Нужно использовать теорему Лагранжа для расчёта тангенциально - вращательных ускорений животных, применить поправочные коэффициенты для пирамидальных интегралов и аппроксимировать систему злополучных носков к квадранту вращательного косинуса.");
		}*/
		if(this.mp.contains("!Доктор")) {
			this.mp.Send("Вам какого: того что принимает опий или того что в ТАРДИС? Впрочем, почти одно и то же. Kappa");
		}
		if(this.mp.contains("!ринсвинд")) {
			int rnd = 1 + (int)(Math.random() * ((5 - 1) + 1));
			String answer = null;
			switch(rnd) {
				case 1:
					answer = "...и Братство Очковтирателей!..";
					break;
				case 2:
					answer = "...и компьютер друидов!..";
					break;
				case 3:
					answer = "...и Двацветок!..";
					break;
				case 4:
					answer = "...и Космозоологи!..";
					break;
				default:
					answer = "...и Дедушка Тролль!..";
			}
			this.mp.Send(answer);
			answer = null;
		}
		if(this.mp.contains("!ТАРДИС")) {
			String tardis = "Пусть это будет... ";
			int rnd = 0 + (int)(Math.random() * ((3 - 0) + 1));
			switch(rnd) {
				case 1:
					tardis += "Время и относительные измерения в пространстве";
					break;
				case 2:
					tardis += "Тонкая Адекватно Резонирующая Диполярная Индукционная Система";
					break;
				case 3:
					tardis += "Тонирующая Асимметричная Редукционно - Дезинсекционная Истинная Семинария";
					break;
				default:
					tardis += "Я не знаю... Придумайте сами...";
			}
			this.mp.Send(tardis);
			tardis = null;
		}
	}
}
