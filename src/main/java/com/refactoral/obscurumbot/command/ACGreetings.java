package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;

/**
 * Greet all known peoples
 * 
 * @author GodusX
 *
 */
public final class ACGreetings extends AbstractCommand {
	public static boolean seenGodusx = false;
	public static boolean seenBotThis = false;

	@Override
	public String getCommandTrigger() {
		return null;
	}
	
	@Override
	public void performCommand(String message, String sender, String channel) {
		if (!sender.toLowerCase().contains(channel.substring(1).toLowerCase())) {
			if (sender.toLowerCase().contains("godusx")) {
				if (!seenGodusx) {
					seenGodusx = true;
					//BotHelper.messageBot("Создатель! Моё почтение!");
				}
			}
		}
	}
	
	@Override
	public boolean trigger(String command, String sender, String channel) {
		return true;
	}

}
