package com.refactoral.obscurumbot.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.ObscurumBot;

public class CTop extends AbstractCommand {
	private List<Long> l;
	private List<String> u;
	Map<Integer, Long> result;
	private int cntr;
	private String topString;

	@Override
	public String getCommandTrigger() {
		return "!топ";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		l = new ArrayList<Long>();
		u = new ArrayList<String>();
		Map<Integer, Long> m = new HashMap<Integer, Long>();
		ObscurumBot.uw.forEach((name, entry) -> {
			l.add(entry.wallet);
			u.add(name);
		});
		cntr = 0;
		do {
			m.put(cntr, l.get(cntr));
			cntr++;
		} while (cntr < l.size());
		cntr = 0;
		topString = "";
		result = new LinkedHashMap<>();
		Stream<Map.Entry<Integer, Long>> st = m.entrySet().stream();
		st.sorted(Map.Entry.comparingByValue()).forEachOrdered(e -> result.put(e.getKey(), e.getValue()));
		result.forEach((key, val) -> {
			if (cntr < 5) {
				topString += u.get(key) + ": " + val + " | ";
			}
		});
		l.clear();
		u.clear();
		m.clear();
		cntr = 0;
		result.clear();
		l = null;
		u = null;
		result = null;
		m = null;
		mp.Send("Топ 5 самых богатых - " + topString);
		topString = null;
	}

}
