package com.refactoral.obscurumbot.command;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;

/**
 * Saves data for all commands and serializes threads
 * 
 * @author GodusX
 *
 */
public final class CSave extends AbstractCommand {
	String channel = null;
	
	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "!save";
	}
	File f;
	File d;
	ObjectOutputStream out;
	FileOutputStream fos;
	@Override
	public void performCommand(String message, String sender, String ch) {
		Log.info("Saving data");
		channel = new StringCrypter().encrypt(ch);
		Main.frame.status.setStatus(Opcodes.x06);
		File tmp = new File("data/comm");
		if(!tmp.exists()) {
			tmp.mkdirs();
		}
		d = new File("data/serialized/" + channel.substring(2) + ".dat");
		f = new File("data/serialized/" + channel.substring(2) + ".szo");
		tmp = null;
		for (AbstractCommand c : ObscurumBot.command) {
			File f = new File("data/comm/" + c.getClass().getName().substring(36) + "-" + channel + ".dat");
			c.onDataSave(f);
		}
		tmp = new File("data/serialized");
		Log.info("Serializing user data to file");
		try {
			if (!tmp.isDirectory()) {
				tmp.delete();
			}
			if (!tmp.exists()) {
				tmp.mkdirs();
			}
			if (f.exists()) {
				f.delete();
			}
			tmp = null;
			if (!f.exists()) {
				f.createNewFile();
			}
			if (d.exists()) {
				d.delete();
			}
			if (!d.exists()) {
				d.createNewFile();
			}
			fos = new FileOutputStream(d);
			out = new ObjectOutputStream(new FileOutputStream(f));
			ObscurumBot.uw.forEach((name, thr) -> {
				try {
					out.writeObject(thr);
					fos.write(name.getBytes());
					fos.write("\n".getBytes());
				} catch (Exception e) {
					Log.fatal(e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}
			});
			out.flush();
			fos.flush();
			out.close();
			fos.close();
			out = null;
			f = null;
			d = null;
		} catch (Exception e) {
			Log.fatal(e.getMessage());
			e.printStackTrace();
		}
		Log.info("Done!");
		Main.frame.status.setStatus(Opcodes.x04);
	}
}
