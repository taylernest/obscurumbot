package com.refactoral.obscurumbot.command;

import java.util.Calendar;

import com.refactoral.obscurumbot.core.AbstractCommand;

/**
 * Displays current date and time
 * @author GodusX
 * TODO: Finish this command
 */
public final class CDate extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!дата";
	}

	public String[] getCommandAlias() {
		return new String[] { "дата", "date", "какая сегодня дата" };
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		Calendar c = Calendar.getInstance();
		this.mp.Send("Сейчас " + c.getTime().toString());
	}
}
