package com.refactoral.obscurumbot.command;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;

import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;

public class CIgnore extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!ignore";
	}

	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		String command = message.toLowerCase().substring(8);
		if(command.contains("-r")) {
			BotHelper.ignore.remove(command.substring(3));
		} else {
			if(command.contains(" ")) return;
			BotHelper.ignore.add(command);
		}
	}

	@Override
	public void onDataSave(File dir) {
		if(dir.isDirectory()) {
			dir.delete();
		}
		if(!dir.exists()) {
			try {
				dir.createNewFile();
			} catch (IOException e) {
				Log.error("Can't create command file");
			}
		}
		Charset charset = Charset.forName("UTF-8");
		String MassStr = "";
		for(String s : BotHelper.ignore) {
			MassStr += (s + "\n");
		}
		try (BufferedWriter writer = Files.newBufferedWriter(dir.toPath(), charset)) {
		    writer.write(MassStr, 0, (MassStr.length() - 1));
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}
		MassStr = null;
		charset = null;
	}

	@Override
	public void onDataLoad(File dir) {
		if (!dir.exists() || dir.isDirectory()) {
			return;
		}
		Charset charset = Charset.forName("UTF-8");
		try (BufferedReader reader = Files.newBufferedReader(dir.toPath(), charset)) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				BotHelper.ignore.add(line);
			}
			line = null;
		} catch (IOException x) {
			Log.error("Can't load data from ignore file!");
		}
		charset = null;
	}
}
