package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;

public class CAkella extends AbstractCommand {

	@Override
	public String getCommandTrigger() {
		return "!акелла";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		mp.Send("Привет. Меня зовут Илья - но лучше Акелла. Мне - 27 лет от роду. Живу в Белоруси. Город Борисов. Люблю играть в майн и некоторые ММО. А так же старые игры, которые еще на 98 летали ))) Из музыки - рок и металл группы 80-90хх.");
	}

}
