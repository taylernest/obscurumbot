package com.refactoral.obscurumbot.command;

import java.io.Serializable;
import java.util.StringTokenizer;

import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.struct.UserWallet;
import com.refactoral.obscurumbot.core.utils.BotHelper;

public final class CWallet extends AbstractCommand implements Serializable {

	@Override
	public String getCommandTrigger() {
		return "!попкорн";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		UserWallet u;
		if (message.length() > getCommandTrigger().length()) {
			boolean cont = false;
			for(short i = 0; i < BotHelper.mods.length; i++) {
				if(sender.equals(BotHelper.mods[i])) {
					cont = true;
					break;
				}
			}
			if(!cont) {
				return;
			}
			try {
				StringTokenizer str = new StringTokenizer(message);
				if (str.countTokens() > 3) {
					return;
				}
				str.nextToken();
				String s = str.nextToken();
				if (s.equalsIgnoreCase("@all")) {
					long l = Long.parseLong(str.nextToken());
					ObscurumBot.uw.forEach((name, usr) -> {
						usr.wallet += l;
					});
					return;
				}
				if((u = ObscurumBot.uw.get(sender)) != null) {
					long tmp = Long.parseLong(str.nextToken());;
					u.wallet += tmp;
					if(tmp > 0) {
						mp.Send("@" + sender + " получил " + tmp + " штучек попкорна");
					} else if (tmp < 0) {
						mp.Send("@" + sender + " потерял " + Math.abs(tmp) + " штучек попкорна");
					}
				}
				return;
			} catch (Exception e) {
				return;
			}
		}
		if((u = ObscurumBot.uw.get(sender)) != null) {
			mp.Send("@" + sender + " имеет " + u.wallet + " штучек попкорна");
		}
	}

}
