package com.refactoral.obscurumbot.command;

import java.lang.management.ManagementFactory;
import java.text.NumberFormat;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;

/**
 * Main bot command for getting some info
 * 
 * @author GodusX
 */
public final class ACSysInfo extends AbstractCommand {
	@Override
	public String getCommandTrigger() {
		return null;
	}
	
	@Override
	public boolean trigger(String message, String sender, String channel) {
		return true;
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		if (!message.toLowerCase().startsWith("!bot")) {
			return;
		}
		boolean mod = false;
		for (String s : BotHelper.mods) {
			if (sender.toLowerCase().equals(s)) {
				mod = true;
				break;
			}
		}
		if(!mod) {
			return;
		}
		if (message.toLowerCase().contains("log")) {
			if (Main.log != null) {
				try {
					long weight = Main.log.length();
					double mbWeight = (((double) weight / 1024D) / 1024D);
					this.mp.Send("Размер логов: " + String.format("%.3fMb", mbWeight));
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				this.mp.Send("Логов нет... ");
			}
			return;
		}

		if (message.toLowerCase().contains("ram")) {
			NumberFormat format = NumberFormat.getInstance();

			long maxMemory = Runtime.getRuntime().maxMemory();
			long allocatedMemory = Runtime.getRuntime().totalMemory();
			long freeMemory = Runtime.getRuntime().freeMemory();
			long currentUsed = allocatedMemory - freeMemory;
			this.mp.Send("Использование памяти: " + format.format(currentUsed / 1024 / 1024) + "Mb of "
					+ format.format(maxMemory / 1024 / 1024) + "Mb (" + (currentUsed * 100L / maxMemory) + "%)");

			return;
		}
		if (message.toLowerCase().contains("cpu")) {

			try {
				MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
				ObjectName name = ObjectName.getInstance("java.lang:type=OperatingSystem");
				AttributeList list = mbs.getAttributes(name, new String[] { "ProcessCpuLoad" });
				if (list.isEmpty()) {
					return;
				}
				Attribute att = (Attribute) list.get(0);
				double value = (double) att.getValue();
				double percentageUsed = ((int) (value * 1000) / 10D);
				this.mp.Send("Использование процессора: " + percentageUsed + "%");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}
	}
}
