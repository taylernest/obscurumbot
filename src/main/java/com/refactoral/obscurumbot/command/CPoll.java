package com.refactoral.obscurumbot.command;

import java.util.ArrayList;
import java.util.HashMap;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.Mode;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;

public class CPoll extends AbstractCommand {
	public static HashMap<String, Integer> results;
	public static ArrayList<String> voted;

	@Override
	public String getCommandTrigger() {
		return "!опрос";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		BotHelper.poll = message.substring(getCommandTrigger().length() + 1);
		Log.info("Initiated poll sequence");
		Main.obot.mode = Mode.POLL;
		Main.obot.waiting = true;
		Main.frame.status.setStatus(Opcodes.x07);
		mp.Send("Новый опрос инициирован, ожидание вариантов. Введите !варианты (вариант 1) (вариант 2) ...");
	}
}
