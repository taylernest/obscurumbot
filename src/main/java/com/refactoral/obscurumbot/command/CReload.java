package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;

/**
 * Reloads bot
 * @author GodusX
 *
 */
public final class CReload extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "!reload";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		Main.obot.reload();
		BotHelper.messageBot("Перезагрузка завершена!");
	}
}
