package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.core.AbstractCommand;

public class CMistik extends AbstractCommand {
	@Override
	public String getCommandTrigger() {
		return "!мистик";
	}
	@Override
	public void performCommand(String message, String sender, String channel) {
		mp.Send("Привет. Меня зовут Сергей (Мистик). Мне 42 года, живу в городе Старый Оскол (Россия). Люблю играть в разного рода игры, но в основном предпочитаю стратегии и песочницы. Никому не в обиду, но не люблю Доту и CS:GO");
	}
}
