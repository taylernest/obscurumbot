package com.refactoral.obscurumbot.command;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.struct.UserWallet;
import com.refactoral.obscurumbot.core.utils.BotHelper;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.core.utils.crypto.StringCrypter;

/**
 * Loads commands data and deserializes threads
 * @author GodusX
 *
 */
public final class CLoad extends AbstractCommand {
	String channel = null;

	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "!load";
	}
	
	ArrayList<String> names;
	ArrayList<UserWallet> users;
	@Override
	public void performCommand(String message, String sender, String ch) {
		channel = new StringCrypter().encrypt(ch);
		Log.info("Loading data");
		Main.frame.status.setStatus(Opcodes.x03);
		File f = new File("data/comm");
		if(!f.exists()) {
			f.mkdirs();
		}
		for (AbstractCommand c : ObscurumBot.command) {
			File fi = new File("data/comm/" + c.getClass().getName().substring(36) + "-" + channel + ".dat");
			c.onDataLoad(fi);
		}
		Log.info("Deserializing data");
		names = new ArrayList<String>();
		users = new ArrayList<UserWallet>();
		try {
			f = new File("data/serialized");
			if(!f.isDirectory()) {
				f.delete();
				BotHelper.messageBot("Загрузка данных завершена, но сериализированных данных не найдено.");
				return;
			}
			if(!f.exists()) {
				f.mkdirs();
				BotHelper.messageBot("Загрузка данных завершена, но сериализированных данных не найдено.");
				return;
			}
			f = null;
			f = new File("data/serialized/" + channel.substring(2) + ".szo");
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
			Boolean b = true;
			while (b) {
				try {
					users.add((UserWallet) in.readObject());
				} catch (EOFException eof) {
					b = false;
				} catch (ClassNotFoundException e) {
					Log.fatal(e.getMessage());
					e.printStackTrace();
					System.exit(1);
				}
			}
			in.close();
			f = null;
			in = null;
			f = new File("data/serialized/" + channel.substring(2) + ".dat");
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				String line = null;
				ObscurumBot.uw.clear();
				ObscurumBot.uw = null;
				while ((line = br.readLine()) != null) {
					names.add(line);
				}
				br.close();
				br = null;
				line = null;
			} catch (Exception e1) {
				BotHelper.messageBot("Загрузка данных завершена, но сериализация не удалась.");
				Log.error(e1.getMessage());
				return;
			}
			f = null;
			ObscurumBot.uw = new HashMap<String, UserWallet>();
			names.forEach(name -> {
				ObscurumBot.uw.put(name, users.get(names.indexOf(name)));
			});
		} catch (Exception e) {
			Log.fatal(e.getMessage());
			e.printStackTrace();
		}
		Log.info("Done!");
		Main.frame.status.setStatus(Opcodes.x05);
	}
}
