package com.refactoral.obscurumbot.command;

import com.refactoral.obscurumbot.Main;
import com.refactoral.obscurumbot.core.AbstractCommand;
import com.refactoral.obscurumbot.core.utils.BotHelper;

/**
 * Class for working with bot delay
 * @author GodusX
 *
 */
public final class CDelay extends AbstractCommand {

	@Override
	public String[] getCommandUsers() {
		return BotHelper.mods;
	}

	@Override
	public String getCommandTrigger() {
		return "!подготовка";
	}

	@Override
	public void performCommand(String message, String sender, String channel) {
		if (message.startsWith(getCommandTrigger() + " ")) {
			Main.obot.globalCooldown = Integer.parseInt(message.substring(getCommandTrigger().length() + 1));
			this.mp.Send("Новый интервал между чтениями: " + Main.obot.globalCooldown + " секунд.");
		}
		this.mp.Send("Интервал чтения комманд: " + Main.obot.globalCooldown + " секунд.");
	}
}
