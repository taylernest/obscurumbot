package com.refactoral.obscurumbot;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.NumberFormat;

import javax.swing.UIManager;

import com.refactoral.obscurumbot.core.ObscurumBot;
import com.refactoral.obscurumbot.core.struct.Config;
import com.refactoral.obscurumbot.core.struct.Locale;
import com.refactoral.obscurumbot.core.struct.Opcodes;
import com.refactoral.obscurumbot.core.utils.Log;
import com.refactoral.obscurumbot.gui.ConnectionFrame;
import com.refactoral.obscurumbot.gui.MainWindow;

/**
 * Main file Starting logging, starting ticking, connecting... Please, read some
 * information. This program licensed under Apache License 2.0. It means you can
 * distribute, modify and use this code as you want. Read more at
 * {@link http://choosealicense.com/licenses/apache-2.0/}
 * 
 * I will use some definitions that you can don't understand correctly. Command
 * container - class, that has method getCommandTrigger() that returns null,
 * overrides trigger and contains many commands inside "performCommand" block. 
 * They usually small, for example, this format useful for easter eggs 
 * (as they will be invisible).
 * 
 * Please, add documentation to your code so other peoples can understand it
 * quickly and don't waste much time. You are welcomed to create forks and
 * make your fixes.
 * 
 * @author GodusX
 */
public class Main {
	public static ObscurumBot obot;
	public static File log = new File("logs/bot.log");
	public static Thread tick, stick;
	public static MainWindow frame;
	public static ConnectionFrame con;
	public static final Charset charset = Charset.forName("UTF-8");
	public static Config cfg;
	public static Locale locale = new Locale("EN");

	/**
	 * Well, you know
	 * @param args command line args.
	 * You can add --start arg to autoload config and start bot
	 * Use -c [server] [port] [name] [pass] [chamber] to autoconnect and add -s arg to save to file
	 */
	public static void main(String[] args) {
		File f1 = new File("logs"), f2 = new File("data");
		try {
			if (!f1.isDirectory()) {
				f1.delete();
				f1.mkdir();
			} else if (!f1.exists()) {
				f1.mkdir();
			}
			if (!f2.isDirectory()) {
				f2.delete();
				f2.mkdir();
			} else if (!f2.exists()) {
				f2.mkdir();
			}
		} catch (Exception ex) {
			Log.error(ex.getMessage());
		}
		f1 = null; f2 = null;
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e1) {
			Log.error(e1.getMessage());
		}
		if (log.exists()) {
			if (log.isDirectory()) {
				log.delete();
			} else {
				byte b = 1;
				for (; b <= 120; b++) {
					if (!(new File("logs/bot-old-" + b + ".log").exists())) {
						break;
					}
				}
				if (b == 120) {
					File f;
					for (; b <= 120; b++) {
						if ((f = new File("logs/bot-old-" + b + ".log")).exists()) {
							f.delete();
						}
					}
					f = null;
					b = 1;
				}
				log.renameTo(new File("logs/bot-old-" + b + ".log"));
			}
		} else {
			try {
				log.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		Log.info("BOT STARTING");
		System.setProperty("file.encoding", "UTF-8");
		Log.info("Loading some system info");
		Runtime r = Runtime.getRuntime();
		NumberFormat format = NumberFormat.getInstance();
		Log.info("Free JVM memory: " + format.format(r.freeMemory() / 1024 / 1024));
		Log.info("Max useable JVM memory: " + format.format(r.maxMemory() / 1024 / 1024));
		Log.info("Avaible processor cores: " + r.availableProcessors());
		Log.info(System.getProperty("os.name") + " | Version: " + System.getProperty("os.version") + " | Architecture: " + System.getProperty("os.arch"));
		r = null;
		format = null;
		con = new ConnectionFrame();
		con.setResizable(false);
		con.setVisible(true);
	}

	public static void exit() {
		frame.onDisabled();
		frame.start.setEnabled(false);
		frame.exit.setEnabled(false);
		obot.stop();
		frame.start.setEnabled(false);
		frame.status.setStatus(Opcodes.x40);
		frame.frame.getContentPane().repaint();
		frame.frame.repaint();
		Log.info("Shutting down program and cleaning up memory");
		ObscurumBot.uw.clear();
		ObscurumBot.messagesToDeliver.clear();
		ObscurumBot.command.clear();
		obot.lastSender = null;
		obot.lastChannel = null;
		ObscurumBot.command = null;
		ObscurumBot.messagesToDeliver = null;
		log = null;
		obot = null;
		tick = null;
		stick = null;
		ObscurumBot.uw = null;
		frame.destroy();
		frame.frame.dispose();
		frame = null;
		System.gc();
	}

	public static void doExit() {
		new Thread(new Runnable() {
			public void run() {
				Main.exit();
			}
		}).start();
	}
	
	/**
	 * Simply sleep with exception handling inside
	 * @param time to sleep in mills
	 */
	public static void sleep(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e1) {
			Log.error(e1.getMessage());
			e1.printStackTrace();
		}
	}
}
